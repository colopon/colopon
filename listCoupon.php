<?php

session_start();
require_once './php/dbconf2.php';

if(!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])){
  header("location: login.php");
  exit;
}
$user_id = $_SESSION['user_id'];
$isLoggedIn = isset($_SESSION['user_id']);
require_once('template/user-header.php');
?>
    <div class="column is-8 is-offset-2">
        <button id="refreshCoupon" onclick ="refreshCoupon(<?php echo $user_id; ?>)" class="button is-primary center">Refresh Coupon</button>
        <br />
        <div id="couponHere">
        <!-- coupon will show up here-->
        </div>
      
    </div>
    <script type='text/javascript' src="./js/listcoupon.js"></script>
    <script>
        refreshCoupon(<?php echo $user_id; ?>);
    </script>
    <div id="modal" class="modal">
      <div class="modal-background"></div>
      <div id="modal-content" class="modal-content">
      </div>
      <button onclick="closeModal()" class="modal-close is-large" aria-label="close"></button>
    </div>
<?php
require_once('template/user-footer.php');
?>
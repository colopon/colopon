<?php
session_start();
require_once './dbconf2.php';
$user_id = $_SESSION['user_id'];
$sql = "SELECT 	user_id,coupon_id,code FROM holder WHERE holder_id= :holder_id";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':holder_id',$_GET['holder_id'],PDO::PARAM_INT);
$stmt->execute();
if($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    if($row['user_id'] === $user_id){
        $code = $row['code'];
        $sql = "SELECT coupon_name,coupon_desc,coupon_img FROM coupon WHERE coupon_id= :coupon_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':coupon_id',$row['coupon_id'],PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $row['code'] = $code;
        $row['holder_id']=$holder_id;
        $sql = "UPDATE holder SET status=0 WHERE holder_id= :holder_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':holder_id',$_GET['holder_id'],PDO::PARAM_INT);
        $stmt->execute();
        echo json_encode($row);
    }
    else{
        die('you is not the owner of this coupon');
    }
}
else{
    die('invalid holder');
}
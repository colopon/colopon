<?php
header("Content-Type: application/json; charset=UTF-8");
require_once './dbconf2.php';
$obj = json_decode($_POST["x"], false);
$vendor_id = $obj->vendor_id;
$count = 1;
$db = $pdo;
$st = $db->query('SELECT coupon_id FROM boxed WHERE vendor_id ='.$obj->vendor_id.'');
if(!$st){
    die("Execute query error ");
}
//success case
else{
    if($st->rowCount() > 0){
        
        $outp = $st->fetchAll(PDO::FETCH_ASSOC);
    }else{
        echo "<h1> You have no coupon.</h1>";
        die();
    }
}
//print_r($outp);
echo '<table class="table is-bordered">';
echo "<thead> 
        <tr> 
            <th>#</th>
            <th>Name</th>
            <th>Description </th>
            <th>Coupon's left </th>
        </tr>
    </thead>";
echo '<tbody>';
foreach ($outp as $couponGot) {
    $couponHaving = $couponGot['coupon_id'];
    $sql = "SELECT * FROM coupon WHERE coupon_id= :coupon_id";
    if($stmt = $db->prepare($sql)){
    $stmt->bindParam(':coupon_id',$couponHaving,PDO::PARAM_INT);
    if($stmt->execute()){
        if($row = $stmt->fetch()){
            //print_r($row);
            echo ' 
            <tr> 
              <th>'.$count.'</th>
              <td>'.$row['coupon_name'].'</td>
              <td>'.$row['coupon_desc'].'</td>
              <td>'.$row['coupon_left'].'</td>
            </tr>
            ';
            
        }
       
    }
    
}
$count++;
}
echo '</table>';
echo '<br />';


?>

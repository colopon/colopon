<?php
require_once('dbconf2.php');
$sql = 'SELECT vendor.vendor_id AS id,vendor.vendor_name AS name,vendor_type.name AS type_name ,location.name AS location_name, vendor.vendor_img as image, vendor.vendor_details as details FROM vendor CROSS JOIN vendor_location ON vendor.vendor_id=vendor_location.vendor_id LEFT JOIN location ON vendor_location.location_id=location.location_id LEFT JOIN vendor_type ON vendor.type=vendor_type.type_id';
if($_GET['type'] && $_GET['location']){
    $sql.=' WHERE vendor.type= :type AND location.location_id= :location';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':type', $_GET['type'], PDO::PARAM_INT);
    $stmt->bindParam(':location', $_GET['location'], PDO::PARAM_INT);
}
else if($_GET['type']){
    $sql.=' WHERE vendor.type= :type';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':type', $_GET['type'], PDO::PARAM_INT);
}
else if($_GET['location']){
    $sql.=' WHERE location.location_id= :location';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':location', $_GET['location'], PDO::PARAM_INT);
}
else{
    $stmt = $pdo->prepare($sql);
}
$stmt->execute();
echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));

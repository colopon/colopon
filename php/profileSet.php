<?php
session_start();
require_once './dbconf2.php';
if(!isset($_SESSION['vendor_id']) || empty($_SESSION['vendor_id'])){
    header("location: vendorLogin.php");
    exit;
  }else{
    $vendor_id = $_SESSION['vendor_id'];
    $username = $_SESSION['username'];
}
if($_SERVER["REQUEST_METHOD"] == "POST"){
$target_dir1 = "../vendorImg/";
$vendor_details_setting = trim($_POST["vendor_details"]);
$vendor_name_setting = trim($_POST["vendor_nameset"]);
$location_id = $_POST["vendor_location"];
$vendor_type_setting = $_POST["vendor_type"];
$target_file1 = $target_dir1.basename($_FILES["image"]["name"]);
$imageFileType = pathinfo($target_file1,PATHINFO_EXTENSION);
if(file_exists($target_file1)){
    echo "File already exists.";
}else if($_FILES["image"]["size"]>500000){
    echo "file is too large";
}else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"){
    echo "wrong file type";
}else if(move_uploaded_file($_FILES["image"]["tmp_name"],$target_file1)){
    $upload_done = 1;
}
$sql = "UPDATE vendor SET vendor_name = :vendor_name_setting, vendor_details = :vendor_details_setting,vendor_img = :vendor_img_setting,type=:vendor_type_setting WHERE vendor_id = :vendor_id";
    if($stmt=$pdo->prepare($sql)){
      $stmt->bindParam(':vendor_id',$vendor_id,PDO::PARAM_INT);
      $stmt->bindParam(':vendor_name_setting',$vendor_name_setting,PDO::PARAM_STR);
      $stmt->bindParam(':vendor_details_setting',$vendor_details_setting,PDO::PARAM_STR);
      $stmt->bindParam(':vendor_img_setting',$target_file1,PDO::PARAM_INT);
      $stmt->bindParam(':vendor_type_setting',$vendor_type_setting,PDO::PARAM_INT);
      if($stmt->execute()){
        $sql1 = "INSERT INTO vendor_location (vendor_id, location_id) VALUES (:vendor_id, :location_id)";
        $stmt=$pdo->prepare($sql1);
        $stmt->bindParam(':vendor_id',$vendor_id,PDO::PARAM_INT);
        $stmt->bindParam(':location_id',$location_id,PDO::PARAM_INT);
        $stmt->execute();
        header("location: ../vendor.php");
      }    
    }

}


?>
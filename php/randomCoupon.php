<?php
header("Content-Type: application/json; charset=UTF-8");
$obj = json_decode($_POST["x"], false);
require_once './dbconf2.php';
$db = $pdo;
$st = $db->query('SELECT * FROM coupon WHERE coupon_left > 0 AND coupon_type ='.$obj->coupon_type.'');

if(!$st){
    die("Execute query error");
}

else{
    $outp = array();
    $outp = $st->fetchAll(PDO::FETCH_ASSOC);

}

$couponCount = count($outp);
if($couponCount===0){
  die('No coupon left!');
}
$randomNum = rand(0,$couponCount-1);
$couponChose = $outp[$randomNum];

$coupon_left = $couponChose['coupon_left']-1;
$coupon_id = $couponChose['coupon_id'];
$sql = "UPDATE coupon SET coupon_left = :coupon_left WHERE coupon_id = :coupon_id";
$st1 = $db->query('SELECT vendor_id FROM boxed WHERE coupon_id = '.$coupon_id.'');
$outst1 = $st1->fetch();
$venid = $outst1['vendor_id'];
$st2 = $db->query('SELECT vendor_name,vendor_details FROM vendor WHERE vendor_id='.$venid.'');
$outst2 = $st2->fetch();



if($stmt = $db->prepare($sql)){
  $stmt->bindParam(':coupon_left',$coupon_left,PDO::PARAM_INT);
  $stmt->bindParam(':coupon_id',$coupon_id,PDO::PARAM_INT);
  if($stmt->execute()){
    $characters = '23456789abcdefghijkmnpqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 7; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    $code=$randomString;
    $sql = "INSERT INTO holder (user_id,coupon_id,status,code) VALUES (:user_id, :coupon_id, 1, :code)";
    if($stmt = $db->prepare($sql)){
      $stmt->bindParam(':user_id',$obj->user_id,PDO::PARAM_INT);
      $stmt->bindParam(':coupon_id',$coupon_id,PDO::PARAM_INT);
      $stmt->bindParam(':code',$code,PDO::PARAM_STR);
      if($stmt->execute()){
        $sql = "UPDATE user SET user_random_status = 0 WHERE user_id = :user_id";
        if($stmt = $db->prepare($sql)){
          $stmt->bindParam(':user_id',$obj->user_id,PDO::PARAM_INT);
          if($stmt->execute()){
            echo '<div class="card">';
            echo '<div class="card-image">
                        <figure class="image is-4by3">
                          <img src="'.$couponChose['coupon_img'].'" alt="Image">
                        </figure>
                      </div>';
            echo '<div class="card-content">
                        
                        <div class="media">
                          <div class="media-content">
                            <p class="title is-4">'.$outst2['vendor_name'].'</p>
                            <p class="subtitle is-6">'.$couponChose['coupon_name'].'</p>
                          </div>
                        </div>
                        
                        <div class="content">
                          '.$couponChose['coupon_desc'].'
                          <br>
                         
                        </div>
                      
                      </div>';
            echo '</div>';
          }
        }
      }else{
        print_r("error query insert");
      }
    }
  }else{
    print_r("error query update");
  }
}

?>
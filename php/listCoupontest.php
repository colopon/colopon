<?php
header("Content-Type: application/json; charset=UTF-8");
require_once './dbconf2.php';
$obj = json_decode($_POST["x"], false);
$user_id = $obj->user_id;
$db = $pdo;
$st = $db->query('SELECT coupon_id,holder_id FROM holder WHERE status=1 AND user_id ='.$obj->user_id.'');
if(!$st){
    die("Execute query error ");
    die();
}
//success case
else{
    if($st->rowCount() > 0){
        
        $outp = $st->fetchAll(PDO::FETCH_ASSOC);
    }else{
        echo "You have no coupon.";
        die();
    }
}
foreach ($outp as $couponGot) {
    $couponHaving = $couponGot['coupon_id'];
    $sql = "SELECT * FROM coupon WHERE coupon_id= :coupon_id";
    $st1 = $db->query('SELECT vendor_id FROM boxed WHERE coupon_id = '.$couponHaving.'');
    $outst1 = $st1->fetch();
    $venid = $outst1['vendor_id'];
    $st2 = $db->query('SELECT vendor_name,vendor_details FROM vendor WHERE vendor_id='.$venid.'');
    $outst2 = $st2->fetch();
    if($stmt = $db->prepare($sql)){
    $stmt->bindParam(':coupon_id',$couponHaving,PDO::PARAM_INT);
    if($stmt->execute()){
        if($row = $stmt->fetch()){
            
            echo '<div class="card">';
            echo '<div class="card-image">
                        <figure class="image is-4by3">
                          <img src="'.$row['coupon_img'].'" alt="Image">
                        </figure>
                      </div>';
            echo '<div class="card-content">
                        
                        <div class="media">
                          <div class="media-content">
                            <p class="title is-4">'.$outst2['vendor_name'].'</p>
                            <p class="subtitle is-6">'.$row['coupon_name'].'</p>
                            
                          </div>
                        </div>
                        
                        <div class="content">
                          '.$row['coupon_desc'].'
                          <br>
                          
                        </div>
                      </div>';
            echo '<footer class="card-footer">
                <a class="card-footer-item" onclick="useCoupon('.$couponGot['holder_id'].')">Use this coupon</a>                        </footer>';
            echo '</div>';
            echo '<br />';
        }
       
    }
    
}
}


?>

<?php
session_start();
require_once './php/dbconf2.php';

$isLoggedIn = isset($_SESSION['user_id']);

require_once('template/user-header.php');

$sql = "SELECT * FROM vendor_type";
$stmt = $pdo->prepare($sql);
$stmt->execute();
?>
  <section class="section">
    <div class="dropdown is-hoverable">
      <div class="dropdown-trigger">
        <button class="button" aria-haspopup="true" aria-controls="dropdown-menu">
          <span>ค้นหาร้านอาหาร</span>
          <span class="icon is-small">
            <i class="fa fa-angle-down" aria-hidden="true"></i>
          </span>
        </button>
      </div>
      <div class="dropdown-menu" id="dropdown-menu" role="menu">
        <div class="dropdown-content">
          <a href="javascript:searchType(0)" class="dropdown-item">
            All
          </a>
          <?php
          while($row = $stmt->fetch()){
          ?>
          <a href="javascript:searchType(<?= $row['type_id'] ?>)" class="dropdown-item">
            <?= $row['name'] ?>
          </a>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
<?php
$sql = "SELECT * FROM location";
$stmt = $pdo->prepare($sql);
$stmt->execute();
?>
    <div class="dropdown is-hoverable">
      <div class="dropdown-trigger">
        <button class="button" aria-haspopup="true" aria-controls="dropdown-menu">
          <span>ค้นหาสถานที่</span>
          <span class="icon is-small">
            <i class="fa fa-angle-down" aria-hidden="true"></i>
          </span>
        </button>
      </div>
      <div class="dropdown-menu" id="dropdown-menu" role="menu">
        <div class="dropdown-content">
          <a href="javascript:searchLocation(0)" class="dropdown-item">
            All
          </a>
          <?php
          while($row = $stmt->fetch()){
          ?>
          <a href="javascript:searchLocation(<?= $row['location_id'] ?>)" class="dropdown-item">
            <?= $row['name'] ?>
          </a>
          <?php
          }
          ?>
        </div>
      </div>
    </div>    
  </section>
<div class="container profile">
      <div class="spacer"></div>
      <div class="columns is-multiline is-centered" id="searchResult">
        
      </div>
</div>
<script src="js/index2.js"></script>
<?php
require_once('template/user-footer.php');
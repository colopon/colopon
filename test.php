<?php


if($_SERVER["REQUEST_METHOD"] == "POST"){
$target_dir1 = "vendorImg/";
$target_file1 = $target_dir1.basename($_FILES["image"]["name"]);
print_r($target_file1);
$imageFileType = pathinfo($target_file1,PATHINFO_EXTENSION);
if(file_exists($target_file1)){
  echo "File already exists.";
}else if($_FILES["vendor_img"]["size"]>500000){
  echo "file is too large";
}else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"){
  echo "wrong file type";
}else if(move_uploaded_file($_FILES["image"]["tmp_name"],$target_file1)){
  $upload_done = 1;
  print_r($upload_done);
}
}
?>


<head>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="./js/jquery.uploadPreview.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $.uploadPreview({
    input_field: "#image-upload",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#image-label",    // Default: .image-label
    label_default: "Choose File",   // Default: Choose File
    label_selected: "Change File",  // Default: Change File
    no_label: false                 // Default: false
  });
});
</script>
<style type="text/css">
#image-preview {
  width: 400px;
  height: 400px;
  position: relative;
  overflow: hidden;
  background-color: #ffffff;
  color: #ecf0f1;
}
#image-preview input {
  line-height: 200px;
  font-size: 200px;
  position: absolute;
  opacity: 0;
  z-index: 10;
}
#image-preview label {
  position: absolute;
  z-index: 5;
  opacity: 0.8;
  cursor: pointer;
  background-color: #bdc3c7;
  width: 200px;
  height: 50px;
  font-size: 20px;
  line-height: 50px;
  text-transform: uppercase;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  text-align: center;
}
</style>

</head>
<body>
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
  
<div id="image-preview">
  <label for="image-upload" id="image-label">Choose File</label>
  <input type="file" name="image" id="image-upload" />
</div>
<div class="field">
          <p class="control">
            <button class="button is-primary">
              submit
            </button>
          </p>
        </div>
</form>
</body>
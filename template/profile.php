<?php
require_once '../php/dbconf2.php';
// $obj = json_decode($_POST["x"], false);
// $vendor_id = $obj->vendor_id;
$vendor_id = $_POST["vendor_id"];
$db = $pdo;
$st = $db->query('SELECT vendor_name,vendor_details,vendor_img,type FROM vendor WHERE vendor_id ='.$vendor_id.'');
$st2 = $db->query('SELECT location_id FROM vendor_location WHERE vendor_id ='.$vendor_id.'');
if(!$st && !$st2){
    die("Execute query error ");
}
//success case
else{
    $outp = $st->fetch();
    $outp1 = $st2->fetchAll(PDO::FETCH_ASSOC);
    $vendor_locate_at=array();
    $vendor_type_is="";
    switch ($outp['type']) {
      case 1:
          $vendor_type_is="Cafe";
          break;
      case 2:
          $vendor_type_is="Dessert";
          break;
      case 3:
          $vendor_type_is="Buffet";
          break;
      case 4:
          $vendor_type_is="A la carte";
          break;
    }
    $vendor_locateAll = array();
    foreach($outp1 as $outpEach){
      switch ($outpEach['location_id']) {
        case 1:
            $vendor_locate_at=array("Paragon");
            break;
        case 2:
            $vendor_locate_at=array("I'm park");
            break;
        case 3:
            $vendor_locate_at=array("Samyarn");
            break;
        case 4:
            $vendor_locate_at=array("Central World");
            break;
        case 5:
            $vendor_locate_at=array("Siam");
            break;
        case 6:
            $vendor_locate_at=array("Chamchuri Square");
            break;
        case 7:
            $vendor_locate_at=array("U Center");
            break;
        case 8:
            $vendor_locate_at=array("Suanluang Square");
            break;
      }
      $vendor_locateAll = array_merge($vendor_locateAll,$vendor_locate_at);
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Colopon - Promote coupon</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bulma.css">



</head>
<body>

<style>
.por-fixed {
    list-style-type: none;
    margin: 0;
    padding: 9;
    overflow: hidden;
    position: fixed;
    bottom: 0;
    color: #fff;
    width: 100%;
    background-color: #ffffff;
    position: fixed;
    box-shadow: 0px -3px 6px rgba(0,0,0,0.16),0px -3px 6px rgba(0,0,0,0.23);

}



.dropdown {
  box-shadow: 0 0 8px #777;
  display: none;
  left: 0;
  position: absolute;
  top: 100%;
  z-index: 1000;
}

.dropdown.is-open {
  display: block;
}


body {
  font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
}
.has-text-muted {
  color: #95A5A6;
}
.fa {
  font-size:10px;
  padding-top:3px;
  color: #95A5A6;
}
.panel-block-item {
  display: inline-block;
  color: #95A5A6;
  font-weight: bold;
  padding: 0 10px;
}
.cart-icon {
  padding-top:10px;
}
.product-header {
  background-color:#fafafa;
}

</style>


<nav class="hero nav has-shadow is-warning ">
    <div class="container  ">
      <div class="nav-left">
        <a class="nav-item">
          <img src="../img/logo.png" alt="Description">
        </a>
      </div>
    </div>





    

    </nav>

   







     
    <div class="section">
    <div>
      <div class="columns">
    
        <div class="column is-6">
          <div class="image is-3by2">
            <img src="<?php echo $outp['vendor_img']; ?>">
          </div>
        </div>
        
        
        <div class="column is-5  container">
          <div class="title is-2"><?php echo $outp['vendor_name'];?></div>
          <hr>
          <br>
          
          <strong>รายละเอียด</strong>
          <br>
          <p>
             <?php echo $outp['vendor_details'];?>
          </p>
          <br>

          <strong>ประเภท</strong>
                <br>
                <p>
                <?php echo $vendor_type_is;?>
                </p>
               <br>
          
               <strong>สถานที่</strong>
                <br>
                <p>
                  <?php
                  foreach($vendor_locateAll as $vendor_loc){
                    echo $vendor_loc;
                    echo "&nbsp; &nbsp;";
                    
                  }
                  ?>
                    
                </p>
               <br>
    
          
            
          </p>
          <br>
    
    
        </div>
      </div>
    </div>
    </div>





</body>
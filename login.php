<?php

require_once './php/dbconf2.php';
 

$username = $password = "";
$username_err = $password_err = "";
 

if($_SERVER["REQUEST_METHOD"] == "POST"){
 

    if(empty(trim($_POST["username"]))){
        $username_err = 'Please enter your username.';
    } else{
        $username = trim($_POST["username"]);
    }

    if(empty(trim($_POST['password']))){
        $password_err = 'Please enter your password.';
    } else{
        $password = trim($_POST['password']);
    }

    if(empty($username_err) && empty($password_err)){

        $sql = "SELECT user_id,username, password FROM user WHERE username = :username";
        
        if($stmt = $pdo->prepare($sql)){

            $stmt->bindParam(':username', $param_username, PDO::PARAM_STR);

            $param_username = trim($_POST["username"]);

            if($stmt->execute()){

                if($stmt->rowCount() == 1){
                    if($row = $stmt->fetch()){
                        $hashed_password = $row['password'];
                        if(password_verify($password, $hashed_password)){

                            session_start();
                            $_SESSION['user_id'] = $row['user_id'];     
                            header("location: index.php");
                        } else{
      
                            $password_err = 'The password you entered was wrong.';
                        }
                    }
                } else{

                    $username_err = 'No account found.';
                }
            } else{
                echo "Please try again later.";
            }
        }
        

        unset($stmt);
    }
    

    unset($pdo);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>Colopon</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bulma.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>


<body>
  <section class="hero is-fullheight">
    <div class="hero-heading bg-2 box-1" >
      <div class=" has-text-centered">
        <img src="img/logo-full.png" width="150px" />

      </div>
    </div>

    <div class="hero-body bg-3 box-2">
      <div class="column is-8 is-offset-2">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
          <div class="login-form">
          
            <p>
              Don't Have CoLopon account <a href="register.php">Sign Up</a>
            </p>
            <br />
            <!-- email(username) -->
            <div class="field">
              <label class="label">Email</label>
              <p class="control has-icons-left">
                <input name="username" class="input <?php echo (!empty($username_err)) ? 'is-danger' : ''; ?>" type="email" placeholder="example@colopon.com" value="<?php echo $username; ?>">
                <span class="icon is-small is-left">
                  <i class="fa fa-envelope"></i>
                </span>
                <span class="icon is-small is-right">
                    <i class="<?php echo (!empty($username_err)) ? 'fa fa-warning' : ''; ?>"></i>
                </span>
                <p class="<?php echo (!empty($username_err)) ? 'help is-danger' : ''; ?>"><?php echo (!empty($username_err)) ? 'ERROR' : ''; ?></p>
              </p>
            </div>
            <!-- password -->
            <div class="field">
              <label class="label">Password</label>
              <p class="control has-icons-left">
                <input name="password" class="input <?php echo (!empty($password_err)) ? 'is-danger' : ''; ?>" type="password" placeholder="******" >
                <span class="icon is-small is-left">
                  <i class="fa fa-lock"></i>
                </span>
                <span class="icon is-small is-right">
                    <i class="<?php echo (!empty($password_err)) ? 'fa fa-warning' : ''; ?>"></i>
                  </span>
                </p>
                <p class="<?php echo (!empty($password_err)) ? 'help is-danger' : ''; ?>"><?php echo $password_err; ?></p>
              </p>
            </div>
            <!-- login button -->
            <div class="field">
              <p class="control">
                <button class="button is-white">
                  Login
                </button>
              </p>
            </div>
      
            <br />
            
          </div>
        </form>
        <a href="./vendorRegis.php"><button class="button is-primary  is-fullwidth" >For Vender</button></a>
      </div>
    </div>
  </section>
  
</body>
</html>
var type=0;
var loca=0;
search()
function searchType(id){
    type=id
    search()
}

function searchLocation(id){
    loca=id
    search()
}
function search(){
    var paramsString = 'type='+type+'&location='+loca
    fetch('php/search.php?'+paramsString)
    .then(x=>x.json())
    .then(data=>{
        var resultDiv = document.querySelector('#searchResult')
        resultDiv.innerHTML=''
        data.forEach(x=>{
            var id = x.id
            var name = x.name
            var type = x.type_name
            var loca = x.location_name
            var image = x.image
            var details = x.details
            var html=`
            <div class="column x-is-1-mobile is-hidden-tablet">
            </div>
            <div class="column is-10-mobile is-one-third-tablet is-one-quarter-desktop" style="margin:auto">
              <a target="_blank" href="./template/profileGet.php?vendor_id=${id}">
                <div class="card">
                  <div class="card-image">
                    <figure class="image is-4by3">
                      <img src="${image}" alt="${name} image">
                    </figure>
                  </div>
                  <header class="card-header">
                    <p class="card-header-title">
                      ${name}
                    </p>
                  </header>
                  <div class="card-content">
                    <div class="content">
                      <p>
                        ${details}
                      </p>
                      <p class="subtitle">
                        ${loca} - ${type}
                      </p>
                      
                    </div>
                  </div>
                </div>
                </a>
              </div>
            `
            resultDiv.innerHTML+=html
        })
    })
}
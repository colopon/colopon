
function openList(id) {
	links = document.getElementsByClassName("close");
	for (var i = 0; i < links.length; i++) {
		links.item(i).style.display = "none";
	}
	document.getElementById(id + "Div").style.display = "block";
	document.getElementById("welcomeDiv").style.display = "none";
}

function buttonActive(element){
    links = document.getElementsByTagName("li");
    for (var i = 0; i < links.length; i++){
        links.item(i).classList.remove("is-active");
    }
    element.classList.add("is-active");
   
}

function buttonActiveTagA(element){
  links = document.getElementsByClassName("menuSide");
  for (var i = 0; i < links.length; i++){
        links.item(i).classList.remove("is-active");
  }
  element.classList.add("is-active");
}

jQuery(document).ready(function ($) {

  var $toggle = $('#nav-toggle');
  var $menu = $('#nav-menu');

  $toggle.click(function() {
    $(this).toggleClass('is-active');
    $menu.toggleClass('is-active');
  });

  $('.modal-button').click(function() {
    var target = $(this).data('target');
    $('html').addClass('is-clipped');
    $(target).addClass('is-active');
  });

  $('.modal-background, .modal-close').click(function() {
    $('html').removeClass('is-clipped');
    $(this).parent().removeClass('is-active');
  });

  $('.modal-card-head .delete, .modal-card-foot .button').click(function() {
    $('html').removeClass('is-clipped');
    $('#modal-ter').removeClass('is-active');
  });

});

// (function() {
//   var burger = document.querySelector('.nav-toggle');
//   var menu = document.querySelector('.nav-menu');
//   burger.addEventListener('click', function() {
//       burger.classList.toggle('is-active');
//       menu.classList.toggle('is-active');
//   });
// })();

$(document).ready(function() {
  $.uploadPreview({
    input_field: "#image-upload",
    preview_box: "#image-preview",
    label_field: "#image-label"
  });
});

function manageCoupon(vendor_id){
  obj = {"vendor_id": vendor_id}
  dbParam = JSON.stringify(obj);
  xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function(){
      if (this.readyState == 4 && this.status == 200) {
         
          document.getElementById("boxedHere").innerHTML = this.responseText;
      }
  };
  xmlhttp.open("POST", "./php/manageCoupon.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send("x=" + dbParam);

}

function previewProfile(vendor_id){
  obj = {"vendor_id": vendor_id}
  dbParam = JSON.stringify(obj);
  xmlhttp = new XMLHttpRequest();
  // xmlhttp.onreadystatechange = function(){
  //     if (this.readyState == 4 && this.status == 200) {
         
  //         document.getElementById("previewProfileHere").innerHTML = this.responseText;
  //     }
  // };
  xmlhttp.open("POST", "./template/vendor-profile.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send("x=" + dbParam);

}

function checkValidate(){
  holderID = document.getElementById("holderId").value;
  couponCODE = document.getElementById("couponCode").value;
  obj = {"holder_id": holderID,"code":couponCODE};
  dbParam = JSON.stringify(obj);
  xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function(){
      if (this.readyState == 4 && this.status == 200) {
         
          document.getElementById("validate").innerHTML = this.responseText;
      }
  };
  xmlhttp.open("POST", "./php/checkValidate.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send("x=" + dbParam);

}


function refreshCoupon(user_id){
    obj = {"user_id": user_id}
    dbParam = JSON.stringify(obj);
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200) {
           
            document.getElementById("couponHere").innerHTML = this.responseText;
        }
    };
    xmlhttp.open("POST", "./php/listCoupontest.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("x=" + dbParam);
}

function useCoupon(holderID){
    fetch('php/useCoupon.php?holder_id='+holderID,{ credentials: 'include' })
    .then(x=>x.json())
    .then(x=>{
        $("#modal").addClass('is-active');
        $("#modal-content").html(`
        <div class="box">
            <h1>Coupon redeemed : ${holderID}</h1>
              <div class="title">${x['coupon_name']}</div>
              <img src="${x['coupon_img']}">
              <br>
              <p>
              ${x['coupon_desc']}
              </p>
              <br>
              Code: <code style="font-size:2em">${x['code']}</code>
          </div>
        `)
    })
}

function closeModal(){
    $("#modal").removeClass('is-active')
}
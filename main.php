<?php
session_start();
require_once './php/dbconf2.php';
$isLoggedIn = isset($_SESSION['user_id']);
$user_id = $_SESSION['user_id'];

$sql = "SELECT user_random_status FROM user WHERE user_id = :user_id";
if($stmt = $pdo->prepare($sql)){
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    if($stmt->execute()){
        if($row = $stmt->fetch()){
            $randomStatus = $row['user_random_status'];
            if($randomStatus == 0){
                header("location: listCoupon.php");
             
            }
        }
    }
}

if(!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])){
  header("location: login.php");
  exit;
}
require_once('template/user-header.php');
?>
  <section class="section">
    <div class="container">
      <figure class="image is-4by3">
        <img src="./img/randomdec.jpg">
      </figure>
      <h1 class="title">Random coupon</h1>
      <div class="columns card-padding">
      <!--Show coupon that is randoming -->
      <div class="column is-8 is-offset-2">
        
        <div id="couponHere">
          <!-- coupon will show up here-->
        </div>
        <br />
        <button id="randomButton" onclick ="onClickRandom(<?php echo $user_id; ?>)" class="button is-primary center is-large">Random</button>
        <br />
        <a href="./listCoupon.php"><button class="button is-primary center is-large">Show my coupon!</button></a>
      </div>
  
    </div>
  </div>
  </section>
  <script src="js/main.js"></script>
<?php
require_once('template/user-footer.php');
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Colopon - Promote coupon</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="./css/bulma.css">
</head>
<body>

<style>
.por-fixed {
    list-style-type: none;
    margin: 0;
    padding: 9;
    overflow: hidden;
    position: fixed;
    bottom: 0;
    color: #fff;
    width: 100%;
    background-color: #ffffff;
    position: fixed;
    box-shadow: 0px -3px 6px rgba(0,0,0,0.16),0px -3px 6px rgba(0,0,0,0.23);

}
.dropdown {
  box-shadow: 0 0 8px #777;
  display: none;
  left: 0;
  position: absolute;
  top: 100%;
  z-index: 1000;
}
.dropdown.is-open {
  display: block;
}
body {
  font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
}
.has-text-muted {
  color: #95A5A6;
}
.fa {
  font-size:10px;
  padding-top:3px;
  color: #95A5A6;
}
.panel-block-item {
  display: inline-block;
  color: #95A5A6;
  font-weight: bold;
  padding: 0 10px;
}
.cart-icon {
  padding-top:10px;
}
.product-header {
  background-color:#fafafa;
}

</style>


<nav class="hero nav has-shadow is-warning ">
    <div class="container  ">
      <div class="nav-left">
        <a class="nav-item">
          <img src="https://www.udemy.com/staticx/udemy/images/v5/logo-green.svg" alt="Description">
        </a>
      </div>
    </div>





    <div class="tabs is-centered" >


          <ul class="por-fixed " >
           <a style="color:#fff;" class="button is-primary is-fullwidth">กดใช้คูปอง</a>



          </ul>
      </div>

    </nav>

    <div class="tabs is-centered" >
          <ul>
            <li id="1" onclick="buttonActive(this)" class="is-active"><a>Promotion</a></li>
            <li id="2" onclick="buttonActive(this)"><a>ร้านอาหาร</a></li>
            <li id="3" onclick="buttonActive(this)"><a>ไปที่ไหน</a></li>

          </ul>
    </div>







     
       <div class="section">
         <div>
           <div class="columns">

             <div class="column is-6">
               <div class="image is-3by2">
                 <img src="https://www.pizzahut.co.uk/order/routes/home/promos/FiftyPercentOff15.069374d6c2613a471fdc632c1336d031.jpg">
               </div>
             </div>
             
             
             <div class="column is-5  container">
               <div class="title is-2">Pizza Hut</div>
               <p class="title is-3 has-text-muted"> 250 Baht -> 210 </p>
               <hr>
               <br>
               <p class="">
                 <i class="fa fa-star title is-5" style="color:#ed6c63"></i>
                 <i class="fa fa-star title is-5" style="color:#ed6c63"></i>
                 <i class="fa fa-star title is-5" style="color:#ed6c63"></i>
                 <i class="fa fa-star title is-5"></i>
                 <i class="fa fa-star title is-5"></i>
                 &nbsp; &nbsp;
                 <strong>41 Reviews</strong>
                 &nbsp; &nbsp;

               </p>

               <strong>รายละเอียด</strong>
               <br>
               <p>
                  บางกรอบ คอมโบ
                  ใหม่! อร่อยจุใจกับพิซซ่าบางกรอบถึง 2 ถาด การ์ลิคไบท์ และ ชีสซี่ป๊อบ เฉพาะออนไลน์เท่านั้นigula consequat gravida ornare.
               </p>
               <br>

               <strong>เงื่อนไข</strong>
                <br>
                <p>
                    มารับประทานทานช่วงเวลา 14-17 น จนถึงวันที่ 31 กันยายน 2560
                </p>
               <br>

               <strong>สถานที่</strong>
                <br>
                <p>
                    พารากอน ชั้น G
                </p>
               <br>
                
               </p>
               <br>


             </div>
           </div>
         </div>
       </div>





  <script async type="text/javascript" src="js/bulma.js"></script>
</body>
</html>
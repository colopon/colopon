<?php
session_start();
require_once './php/dbconf2.php';

$vendor_name_setting = $coupon_name = $coupon_desc = $vendor_details = '';

if(!isset($_SESSION['vendor_id']) || empty($_SESSION['vendor_id'])){
  header("location: vendorLogin.php");
  exit;
}else{
  $vendor_id = $_SESSION['vendor_id'];
  $username = $_SESSION['username'];
}

if($_SERVER["REQUEST_METHOD"] == "POST"){
  if(empty(trim($_POST["vendor_nameset"]))){
    $coupon_type = $_POST["coupon_type"];
    $coupon_name = $_POST["coupon_name"];
    $coupon_desc = $_POST["coupon_desc"];
    $coupon_number = $_POST["coupon_number"];
    $target_dir = "coupImg/";
    $target_file = $target_dir.basename($_FILES["couponImg"]["name"]);
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    if(file_exists($target_file)){
      echo "File already exists.";
    }else if($_FILES["couponImg"]["size"]>500000){
      echo "file is too large";
    }else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"){
      echo "wrong file type";
    }else if(move_uploaded_file($_FILES["couponImg"]["tmp_name"],$target_file)){
      $upload_done = 1;
    }


    $sql = "INSERT INTO coupon(coupon_type,coupon_name,coupon_desc,coupon_img,coupon_left) VALUES (:coupon_type,:coupon_name,:coupon_desc,:coupon_img,:coupon_left)";
    $sqlCountCoupon = "SELECT MAX(coupon_id) as coupon_id_latest FROM coupon";
    $sql1 = "INSERT INTO boxed (vendor_id, coupon_id) VALUES (:vendor_id, :coupon_id)";
    if($stmt=$pdo->prepare($sql)){
      $stmt->bindParam(':coupon_type',$coupon_type,PDO::PARAM_INT);
      $stmt->bindParam(':coupon_name',$coupon_name,PDO::PARAM_INT);
      $stmt->bindParam(':coupon_desc',$coupon_desc,PDO::PARAM_INT);
      $stmt->bindParam(':coupon_img',$target_file,PDO::PARAM_INT);
      $stmt->bindParam(':coupon_left',$coupon_number,PDO::PARAM_INT);
      if($stmt->execute()){
        $stmt=$pdo->prepare($sqlCountCoupon);
        $stmt->execute();
        $countCouponfetch = $stmt->fetch();
        $countCoupon = $countCouponfetch['coupon_id_latest'];
        if($stmt=$pdo->prepare($sql1)){
          $stmt->bindParam(':vendor_id',$vendor_id,PDO::PARAM_INT);
          $stmt->bindParam(':coupon_id',$countCoupon,PDO::PARAM_INT);
          $stmt->execute();
        }
        header("location: vendor.php");
      }
    }
  }
}


$sql = "SELECT * FROM vendor WHERE vendor_id = :vendor_id";
if($stmt = $pdo->prepare($sql)){
    $stmt->bindParam(':vendor_id', $vendor_id, PDO::PARAM_INT);
    if($stmt->execute()){
        if($row = $stmt->fetch()){
            $vendor_coupon_left = $row['vendor_coupon_left'];
             $vendor_name = $row['vendor_name'];
            if($vendor_name === NULL){
              $vendor_name = "You haven't set your vendor's name yet!";
            }

           
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>Colopon</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bulma.css">
  <link rel="stylesheet" href="css/aside.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="css/por.css">

  <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="./js/jquery.uploadPreview.min.js"></script>
  <script type='text/javascript' src="./js/vendor.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102149637-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Hotjar Tracking Code for http://colopon.com -->
  <script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:594585,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '2036446063251686'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=2036446063251686&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</head>


<body>

  <!--navbar    -->
 
  <nav class="navbar has-shadow" style="background: #f9ec2a;">
    <div class="navbar-brand">
      <div class="nav-left">
        <a class="nav-item">
          <img src="img/logo.png" alt="colopon">
        </a>
      </div>


      <!-- <span id="nav-toggle" class="nav-toggle" data-target="nav-menu">
      <span class="icon"><i class="fa fa-fw fa-user"></i></span><span class="name"></span>
      </span> -->

      
    </div>

    <div id="navMenuProfile" class="navbar-menu">
      <div class="navbar-end">
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link" >
            <i class="fa fa-fw fa-user"></i>
            <?php echo $username; ?>
          </a>
          <div class="navbar-dropdown">
            <a id="editProfile" onclick="openList(this.id)" class="navbar-item " >
              <i class="fa fa-fw fa-cog"></i>
               &nbsp;Edit Profile
            </a>
            <a class="navbar-item " href="./php/vendorLogout.php">
              <i class="fa fa-fw fa-unlock"></i>
              &nbsp;Log out
            </a>
          </div>
        </div>
      </div>
    </div>
  </nav>

<div class="columns">
  <!-- aside begin-->
  <aside class="column is-3 hero aside is-fullheight is-hidden-mobile">
    
    <div>
      <div class="main">
      <p class="menu-label del-pad-bottom"><b>Profile</b></p>
        <ul class="menu-list">
          <li><a class="menuSide" id="previewProfile" onclick="buttonActiveTagA(this),openList(this.id)">View Profile</a></li>
          
        </ul>
      <p class="menu-label del-pad-bottom"><b>Coupon</b></p>
        <ul class="menu-list">
          <li><a class="menuSide" id="addCoupon" onclick="buttonActiveTagA(this),openList(this.id)">Add Coupon</a></li>
          <li><a class="menuSide" id="editCoupon" onclick="buttonActiveTagA(this),openList(this.id),manageCoupon(<?php echo $vendor_id; ?>)">Manage Coupon</a></li>      
          <li><a class="menuSide" id="checkCoupon" onclick="buttonActiveTagA(this),openList(this.id)">Validate Coupon</a></li>      

        </ul>
      
      </div>
    </div>
  </aside>
  <!-- aside end -->
          
  <div class="column is-5 is-centered is-offset-2">
       
    <div id="welcomeDiv">
      <div class="contt content">
          <h1> Welcome <?php echo $username; ?>! </h1>
          <p>You can edit your vendor's profile at profile tab.</p>
      </div>
    </div>

    <div id="previewProfileDiv" class="close">
      <div class="contt content">
        <form  action="./template/profile.php" method="post">
        <p> You can preview your profile here.</p>
        
          <input type="hidden" name="vendor_id" id="vendor_id" value="<?php echo $vendor_id; ?>">
          <button class="button is-primary" type="submit" id="button">Preview Your Profile</button>
        </form>
        
        
      </div>
    </div>

    <div id="addCouponDiv" class="close">
      <div class="contt content">
          <h1> Add Coupon</h1>
      </div>        
         
      <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
        <div class="field">
          <label class="label">Category</label>
            <p class="control">
              <span class="select">
                  <select name="coupon_type">
                    <option value="1">Restaurant</option>
                    <option value="2">Event</option>
                    <option value="3">Clothing</option>
                    <option value="4">Start Up</option>
                  </select>
              </span>
            </p>    
        </div>

        <div class="field">
          <label class="label">Coupon</label>
            <p class="control">
              <input name="coupon_name" class="input" type="text" placeholder="ex. Sales 50%" value="<?php echo $coupon_name; ?>">
            </p>
        </div>
                  
        <div class="field">
          <label class="label">Description</label>
            <p class="control">
              <textarea name="coupon_desc" class="textarea" placeholder="ex. Holiday's discount! You shouldn't miss."></textarea>
            </p>
        </div>
                  
        <div class="field">
          <label class="label">Coupon Image</label>
            <p class="control">
              <input type="file" name="couponImg" id="couponImg">
              <p class="help">less than 16MB</p>
            </p>
        </div>

        <div class="field">
          <label class="label">Number</label>
            <p class="control">
              <input name="coupon_number" class="input" type="number" placeholder="20" value="<?php echo $coupon_number; ?>">
            </p>
        </div>
                 
        <div class="field">
          <p class="control">
            <button class="button is-primary">
              submit
            </button>
          </p>
        </div>
      </form>
    </div>

    <div id="editCouponDiv" class="close">
      <div class="contt content">
        <h1> Manage Coupon </h1>
        <div id="boxedHere">
          
        </div>
      </div>
    </div>
    
    <div id="checkCouponDiv" class="close">
      <div class="contt content">
        <h1> Validation Coupon </h1>
        <div class="field">
                <label class="label">Holder ID</label>
                  <div class="control">
                    <input id="holderId" name="holder_id" class="input" type="text" placeholder="id" >
                  </div>
        </div>
        <div class="field">
                <label class="label">Coupon's Code</label>
                  <div class="control">
                    <input id="couponCode"name="coupon_code" class="input" type="text" placeholder="code" >
                  </div>
        </div>
        <br />
        <button class="button is-primary center is-large" onclick="checkValidate()">Validate Now!</button>

        <p>
        <div id="validate">
          
        </div>
        </p>
      </div>
    </div>
    

    <div id="editProfileDiv" class="close">
      <br />
        <div class="contt">
          <div class="is-centered">
            <br />
            <form action="./php/profileSet.php" method="post" enctype="multipart/form-data">
              <!-- ven name -->
              <div class="field">
                <label class="label">Vendor's name</label>
                  <div class="control">
                    <input name="vendor_nameset" class="input" type="text" placeholder="name" value="<?php echo $vendor_name_setting; ?>">
                  </div>
              </div>

              <br />
              <!-- ven type -->
              <div class="field">
              <label class="label">Type</label>
                <p class="control">
                  <span class="select">
                      <select name="vendor_type">
                        <option value="1">Cafe</option>
                        <option value="2">Dessert</option>
                        <option value="3">Buffet</option>
                        <option value="4">A la carte</option>
                      </select>
                  </span>
                </p>    
              </div>  

              <br />
              <!-- ven location -->
              <div class="field">
              <label class="label">Location</label>
                <p class="control">
                  <span class="select">
                      <select name="vendor_location">
                        <option value="1">Paragon</option>
                        <option value="2">I'm park</option>
                        <option value="3">Samyarn</option>
                        <option value="4">Central World</option>
                        <option value="5">Siam</option>
                        <option value="6">Chamchuri Square</option>
                        <option value="7">U Center</option>
                        <option value="8">Suanluang Square</option>
                      </select>
                  </span>
                </p>    
              </div>
              
              <br />
              <!-- ven details -->
              <div class="field ">
                <label class="label">Details</label>
                  <div class="control">
                    <textarea name="vendor_details" class="textarea" placeholder="Details"></textarea>
                  </div>
              </div>

              <br />
              <!-- ven upload img -->
              <div class="field is-centered">
                <label class="label">Vendor's image</label>
                  <p class="control">
                  <div id="image-preview">
                    
                    <input type="file" name="image" id="image-upload" />
                  </div>   
                  <label id="image-label"></label>
                  <p class="help">less than 16MB</p>                  </p>
              </div>
          
              <br />
              <!-- submit -->
              <div class="field">
                <p class="control">
                  <button class="button is-primary">
                    Submit
                  </button>
                </p>
              </div>
            </form>
          </div>
        </div>
    </div>  
       



  </div>
</div>

    


</body>

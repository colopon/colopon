<?php

require_once './php/dbconf2.php';


$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";


if($_SERVER["REQUEST_METHOD"] == "POST"){

    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
    } else{

        $sql = "SELECT vendor_id FROM vendor WHERE username = :username";

        if($stmt = $pdo->prepare($sql)){

            $stmt->bindParam(':username', $param_username, PDO::PARAM_STR);


            $param_username = trim($_POST["username"]);


            if($stmt->execute()){
                if($stmt->rowCount() == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }

        unset($stmt);
    }

    if(empty(trim($_POST['password']))){
        $password_err = "Please enter a password.";
    } elseif(strlen(trim($_POST['password'])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = $_POST['password'];
    }

    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = 'Please confirm password.';
    } else{
        $confirm_password = trim($_POST['confirm_password']);
        if($password != $confirm_password){
            $confirm_password_err = 'Password did not match.';
        }
    }

    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){

        $sql = "INSERT INTO vendor (username, password) VALUES (:username, :password)";

        if($stmt = $pdo->prepare($sql)){
            $stmt->bindParam(':username', $param_username, PDO::PARAM_STR);
            $stmt->bindParam(':password', $param_password, PDO::PARAM_STR);

            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash

            if($stmt->execute()){
                header("location: vendorLogin.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }

        unset($stmt);
    }

    unset($pdo);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>Colopon</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bulma.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>


<body>

  <section class="hero is-fullheight">

    <div class="hero-heading bg-2 box-1" >
       <div class=" has-text-centered">
        <img src="img/logo-full.png" width="150px" />

      </div>
    </div>

    <div class="hero-body bg-3 box-2">
      <div class="column is-8 is-offset-2">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

          <div class="login-form">

            <p>

            <label class="label" style="text-decoration: underline;">Sign Up</label>
            </p>


            <!-- email -->
            <div class="field">
              <label class="label">Email</label>
                <p class="control has-icons-left has-icons-right">
                  <input name="username" class="input <?php echo (!empty($username_err)) ? 'is-danger' : ''; ?>" type="text" placeholder="example@colopon.com" value="<?php echo $username; ?>">
                  <span class="icon is-small is-left">
                    <i class="fa fa-envelope"></i>
                  </span>
                  <span class="icon is-small is-right">
                    <i class="<?php echo (!empty($username_err)) ? 'fa fa-warning' : ''; ?>"></i>
                  </span>
                </p>
                <p class="<?php echo (!empty($username_err)) ? 'help is-danger' : 'help'; ?>"><?php echo (!empty($username_err)) ? 'This email is already taken.' : ''; ?></p>
            </div>

            <!-- password -->
            <div class="field">
              <label class="label">Password</label>
                <p class="control has-icons-left has-icons-right">
                  <input name="password" class="input <?php echo (!empty($password_err)) ? 'is-danger' : ''; ?>" type="password" placeholder="******" value="<?php echo $password; ?>">
                  <span class="icon is-small is-left">
                    <i class="fa fa-lock"></i>
                  </span>
                  <span class="icon is-small is-right">
                    <i class="<?php echo (!empty($password_err)) ? 'fa fa-warning' : ''; ?>"></i>
                  </span>
                </p>
                <p class="<?php echo (!empty($password_err)) ? 'help is-danger' : 'help'; ?>"><?php echo (!empty($password_err)) ? 'password is not allowed.' : 'required atleast 6 character'; ?></p>
            </div>
            <!-- Confirm password -->
            <div class="field">
              <label class="label">Confirm Password</label>
                <p class="control has-icons-left has-icons-right">
                  <input name="confirm_password" class="input <?php echo (!empty($confirm_password_err)) ? 'is-danger' : ''; ?>" type="password" placeholder="******" value="<?php echo $password; ?>">
                  <span class="icon is-small is-left">
                    <i class="fa fa-lock"></i>
                  </span>
                  <span class="icon is-small is-right">
                    <i class="<?php echo (!empty($confirm_password_err)) ? 'fa fa-warning' : ''; ?>"></i>
                  </span>
                </p>
                <p class="<?php echo (!empty($confirm_password_err)) ? 'help is-danger' : 'help'; ?>"><?php echo (!empty($confirm_password_err)) ? 'password is not allowed.' : ''; ?></p>
            </div>

            <br />
            <p class="control login ">
              <button class="button is-white " >Sign Up</button>

            </p>


          </div>
        </form>
        <br />
        <p>Already have an account? <a href="vendorLogin.php">LOG IN</a></p>
        <br />


      </div>
    </div>
  </section>

</body>
</html>